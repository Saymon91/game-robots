import React from 'react';
import './App.css';

import World from './components/world';
import Editor from './components/code-editor';
import { Program } from './store/program';
import { RobotsStore } from './store/robots-store';
import { Game } from './store/game';

const program = new Program();
const robots = new RobotsStore(program);
const game = new Game(robots, program);



function App() {
  return (
    <World game={game}>
      <Editor program={program} />
    </World>
  );
}

export default App;
