import { makeObservable, observable, action, computed } from 'mobx';

import type { Direction, RobotConfig, RobotState } from '../code/robot';
import { Program } from './program';
import { Robot } from '../code/robot';
import type { TickCommand } from '../code/robot';

export type IRobotInfo = Pick<RobotConfig, 'name'> & RobotState & { initialPosition: number };
export type UpdateRobot = Partial<RobotState>;

export class RobotInfo implements IRobotInfo {
  @observable public direction: Direction = 'front';
  @observable private positionDiff: number = 0;
  @observable private state: 'wait' | 'run' = 'wait';

  @computed public get image(): string {
    return `${this.name}-${this.state}`
  }

  @computed
  public get position(): number {
    return this.positionDiff + this.initialPosition;
  }

  private readonly robot!: Robot;

  constructor(
    public readonly name: string,
    public readonly initialPosition: number,
    private readonly program: Program,
    initialDirection: Direction = 'front',
  ) {
    makeObservable(this);

    this.direction = initialDirection;
    this.robot = new Robot(name, program.program, {}, initialDirection);
    this.robot.events.subscribe(({ event, payload }) => {
      switch (event) {
        case 'move': {
          this.updateRobot(payload);
        }
      }
    });
  }

  @action
  private updateRobot({ position, direction }: UpdateRobot) {
    position !== undefined && (this.positionDiff = position);
    direction && (this.direction = direction);
  }

  @action
  public tik(params: TickCommand['params']) {
    this.robot.input.next({ command: 'tick', params });
    this.state = 'run';
  }

  @action
  public tok() {
    this.state = 'wait';
  }

  @action
  public reset() {
    const program = this.program.program;
    console.log('Reset robot', this.name, JSON.stringify(program, null, 2));
    this.robot.input.next({
      command: 'reset',
      params: {
        program,
        vars: {},
      }
    });
  }
}
