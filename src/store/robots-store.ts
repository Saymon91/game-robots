import { makeObservable, observable, action, computed } from 'mobx';
import type { RobotConfig } from '../code/robot';
import { RobotInfo } from './robot-info';
import { Program } from './program';

export type AddRobot = Pick<RobotConfig, 'name'> & { initialPosition: number; };

export class RobotsStore {
  @observable
  public robot1?: RobotInfo;
  @observable
  public robot2?: RobotInfo;

  private tokTm?: number;

  @computed
  get isMet(): boolean {
    return this.robot1?.position === this.robot2?.position;
  }

  @computed
  public get robotsList(): RobotInfo[] {
    return [this.robot1, this.robot2].filter(Boolean) as RobotInfo[];
  }

  constructor(private readonly program: Program) {
    makeObservable(this);
  }

  public init([courier, builder]: number[]) {
    courier !== this.robot1?.initialPosition && (this.robot1 = new RobotInfo('courier', courier, this.program, 'right'));
    builder !== this.robot2?.initialPosition && (this.robot2 = new RobotInfo('builder', builder, this.program, 'left'));
  }

  @action
  public reset() {
    this.robot1?.reset();
    this.robot2?.reset();
  }

  @action
  public tick(interval: number) {
    this.robot1?.tik({ ON_PLATFORM: this.robot2?.initialPosition === this.robot1?.position });
    this.robot2?.tik({ ON_PLATFORM: this.robot1?.initialPosition === this.robot2?.position });
    if (this.tokTm) {
      clearTimeout(this.tokTm);
    }
    this.tokTm = setTimeout(() => {
      this.robot1?.tok();
      this.robot2?.tok();
      this.tokTm = undefined;
    }, interval / 2) as unknown as number;
  }
}
