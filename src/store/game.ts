import { action, computed, makeObservable, observable } from 'mobx';
import { interval } from 'rxjs';
import type { Observable, Subscription } from 'rxjs';

import type { RobotsStore } from './robots-store';
import { Program } from './program';

export class Game {
  @observable public stepsBetweenRobots: number = 0;
  @observable public ticks: number = 0;
  @observable public tickInterval: number = 800;
  @observable public stepSize = 0;

  @computed public get tickOn(): boolean {
    return !!this.tickerSubscriber;
  }

  private isInit = false;

  private ticker: Observable<number> = interval(this.tickInterval);
  @observable private tickerSubscriber?: Subscription;

  constructor(
    public readonly robots: RobotsStore,
    public readonly program: Program,
  ) {
    makeObservable(this);
  }

  @action
  public tick() {
    this.ticks++;
    this.robots.tick(this.tickInterval);
  }

  @action
  public reset() {
    this.ticks = 0;
    this.robots.reset();
  }

  private subscribeOnTick() {
    if (!this.robots.isMet) {
      this.tickerSubscriber = this.ticker.subscribe(() => {
        this.ticks++;
        this.robots.tick(this.tickInterval);
        if (this.robots.isMet) {
          this.tickerSubscriber?.unsubscribe();
          this.tickerSubscriber = undefined;
        }
      });
    }
  }

  @action
  public toggleTick() {
    if (this.tickOn) {
      this.tickerSubscriber?.unsubscribe();
      this.tickerSubscriber = undefined;
    } else {
      this.subscribeOnTick();
    }
  }

  @action
  public changeTickInterval(value: number) {
    this.tickInterval = value;
    if (this.tickOn) {
      this.tickerSubscriber!.unsubscribe();
      this.tickerSubscriber = undefined;
      this.ticker = interval(this.tickInterval);
      this.subscribeOnTick();
    }
  }

  @action
  public init(initial: number[]) {
    this.robots.init(initial);
    this.isInit = true;
  }

  @action
  public setStepsBetweenRobots(num: number) {
    this.stepsBetweenRobots = num;
  }

  @action
  public setStepSize(num: number) {
    this.stepSize = num;
  }

  @computed
  public get distanceBetweenRobots(): number {
    return Math.abs((this.robots.robot1?.position ?? 0) - (this.robots.robot2?.position ?? 0));
  }

}
