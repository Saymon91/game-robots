import { observable, makeObservable, action, computed } from 'mobx';
import { cloneDeep } from 'lodash';

import type { Actions } from '../code/robot';
import { commands } from '../components/code-editor';

export class CodeRow {
  @observable public code: string = '';
  @observable public valid = false;
  @observable public action?: Actions;

  constructor(
    code: string,
  ) {
    makeObservable(this);
    this.code = code.toUpperCase();
    this.validate();
  }

  @action
  private validate() {
    for (const [regexp, action] of commands) {
      const data = regexp.exec(this.code);
      if (data) {
        this.valid = true;
        const actionInfo = { action } as Actions;
        data.groups && Object.assign(actionInfo, { params: data.groups });
        this.action = actionInfo;
        return ;
      }
    }

    this.valid = false;
    this.action = undefined;
  }

  @action
  public setCode(code: string) {
    this.code = code.toUpperCase();
    this.validate();
  }
}

export class Program {
  @observable public rows: CodeRow[] = [new CodeRow('')];
  @observable public history: Actions[][] = [];

  @computed get program(): Actions[] {
    return this.rows.reduce<Actions[]>((acc, { action }) => action ? [...acc, action] : acc, []);
  }
  @computed get isValid(): boolean {
    return this.rows.length > 0 && this.rows.every(row => row.valid);
  }

  constructor() {
    makeObservable(this);
  }


  @action
  public editRow(row: Actions, rowIndex: number) {
    this.history.push(this.program.map(cloneDeep));
    this.program[rowIndex] = row;
  }

  @action
  public deleteRow(rowIndex: number) {
    console.log('delete row', rowIndex);
    this.history.push(this.program.map(cloneDeep));
    this.rows.splice(rowIndex, 1);
  }

  @action
  public onEnter(rowIndex: number) {
    if (rowIndex + 1 === this.rows.length) {
      this.rows.push(new CodeRow(''));
    }
  }

  @action
  public clear() {
    this.rows = [new CodeRow('')];
  }
}
