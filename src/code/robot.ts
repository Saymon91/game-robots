import { filter, Observable, Subject } from 'rxjs'
import { cloneDeep } from 'lodash';

export type Direction = 'front' | 'left' | 'right';

interface IRight {
  readonly action: 'RIGHT';
}

interface ILeft {
  readonly action: 'LEFT';
}

interface IGoTo {
  readonly action: 'GOTO';
  readonly params: {
    readonly row_number: number;
  };
}

interface IIf {
  readonly action: 'IF',
  readonly params: {
    readonly check_var: string;
    readonly then_action: IRight['action'] | ILeft['action'] | IGoTo['action'];
    readonly row_number?: number;
  }
}

export type Actions = ILeft | IRight | IGoTo | IIf;

export interface RobotState {
  readonly direction: Direction;
  readonly position: number;
}

export interface RobotConfig {
  readonly name: string;
  readonly program: Actions[];
  readonly image?: string;
}

export interface Move {
  readonly event: 'move',
  readonly payload: RobotState;
}

export interface CodeError {
  readonly event: 'error',
  readonly payload: {
    readonly program: Actions[];
    readonly rowNumber: number;
    readonly message: string;
  };
}

export type RobotEvents = Move;

export type Commands = 'tick';

export interface TickCommand {
  readonly command: 'tick';
  readonly params: Readonly<Record<string, boolean>>;
}

export interface ResetCommand {
  readonly command: 'reset';
  readonly params?: {
    readonly program?: Actions[];
    readonly vars?: Readonly<Record<string, boolean>>;
  }
}

export type RobotCommands = TickCommand | ResetCommand;

export type Vars = Record<string, boolean>;

export class Robot implements RobotState, RobotConfig {
  public events!: Observable<RobotEvents>;
  public input!: Subject<RobotCommands>;

  public get direction(): Direction {
    return this.currentDirection;
  }

  public get position(): number {
    return this.currentPosition;
  }

  private currentPosition!: number;
  private currentDirection: Direction = 'front';
  private currentCodeRow: number = -1;
  private readonly emitter!: Subject<RobotEvents>;
  private initialVars!: Readonly<Vars>;
  private vars!: Vars;
  public program!: Actions[];

  constructor(
    public readonly name: string,
    program: Actions[],
    initialVars: Vars = {} as Vars,
    private initialDirection: Direction = 'front',
  ) {
    this.currentPosition = 0;
    this.currentDirection = initialDirection;
    console.info('init robot', name);
    this.program = program.map(row => Object.freeze(cloneDeep(row)));
    this.initialVars = Object.freeze(Object.create(initialVars));
    this.vars = cloneDeep(this.initialVars);
    this.emitter = new Subject<RobotEvents>()
    this.events = this.emitter.asObservable();
    this.input = new Subject<RobotCommands>();
    this.input.subscribe(console.log)
    this.input
      .pipe(filter(({ command }) => command === 'tick'))
      .subscribe(({ params }) => this.tick(params as Vars));
    this.input
      .pipe(filter(({ command }) => command === 'reset'))
      .subscribe(({ params }) => this.reset(params));
  }

  public reset({ program, vars }: ResetCommand['params'] = {}): RobotState {
    if (program) {
      this.program = program.map(row => Object.freeze(cloneDeep(row)));
    }
    if (vars) {
      this.initialVars = cloneDeep(vars as Vars);
    }
    this.currentCodeRow = -1;
    this.vars = cloneDeep(this.initialVars);
    const state: RobotState = {
      position: (this.currentPosition = 0),
      direction: (this.currentDirection = this.initialDirection ?? 'front'),
    };

    this.emitter.next({
      event: 'move',
      payload: state,
    });
    return state;
  }

  public tick(vars?: Partial<Vars>): RobotState | undefined {
    console.info('tick', this.currentCodeRow)
    if ((this.currentCodeRow + 1) >= this.program.length) {
      this.currentDirection = 'front';
      return {
        position: this.currentPosition,
        direction: this.currentDirection,
      };
    }

    try {
      vars && Object.assign(this.vars, vars);
      this.currentCodeRow++;
      const state = this.exec();
      this.emitter.next({ event: 'move', payload: state });
      return state;
    } catch (err) {
      this.emitter.error({
          message: (err as Error).message,
          rowNumber: this.currentCodeRow,
          program: this.program,
      });
      return undefined;
    }
  }

  private exec(): RobotState {
    console.info('exec', this.currentCodeRow);
    const codeRow = this.program[this.currentCodeRow];
    if (!codeRow) {
      throw new Error(`Row ${this.currentCodeRow}  is not defined in program for robot ${this.name}`);
    }
    // @ts-ignore
    return this[codeRow.action](codeRow.params);
  }

  private doConditionalAction({ action, ...props }: IRight | ILeft | IGoTo): RobotState {
    // @ts-ignore
    return this[action](props.params);
  }

  private RIGHT(): RobotState {
    return {
      position: ++this.currentPosition,
      direction: (this.currentDirection = 'right'),
    };
  }

  private LEFT(): RobotState {
    return {
      position: --this.currentPosition,
      direction: (this.currentDirection = 'left'),
    };
  }

  private GOTO({ row_number }: IGoTo['params']): RobotState | undefined {
    this.currentCodeRow = row_number - 1;
    return this.exec();
  }

  private IF({ check_var, then_action, row_number }: IIf['params']): RobotState | undefined {
    if (this.vars[check_var]) {
      // @ts-ignore
      return this.doConditionalAction({ action: then_action, params: { row_number }});
    } else {
      return this.tick();
    }
  }
}

const registry: Record<string, Robot> = {};

export const createRobot = ({ name, program }: RobotConfig): Robot => {
  return registry[name] ?? (registry[name] = new Robot(name, program));
}
