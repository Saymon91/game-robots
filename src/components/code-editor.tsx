import React, { PropsWithChildren, useEffect, useRef } from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react-lite';

import { CodeRow, Program } from '../store/program';
import { Actions } from '../code/robot';

type Operators = 'LEFT' | 'RIGHT' | 'GOTO' | 'IF';

interface CodeRowProps {
  number: number;
  codeRow: CodeRow;
  deleteRow: () => void;
  onEnter: () => void;
  focus: boolean;
  program: Program;
}

interface ProgramRow {
  code: string;
  valid: boolean;
  action: Actions | null;
}

interface CodeRowWrapperProps {
  valid: boolean;
}

export const useMountEffect = (fun: () => void) => useEffect(fun, []);

const UseFocus = (): [React.RefObject<HTMLInputElement>, () => void] => {
  const htmlElRef = useRef<HTMLInputElement>(null);
  const setFocus = () => { htmlElRef.current &&  htmlElRef.current.focus() };

  return [htmlElRef, setFocus];
}

export const commands = new Map<RegExp, Operators>([
  [/^RIGHT$/, 'RIGHT'],
  [/^LEFT$/, 'LEFT'],
  [/^GOTO\s+(?<row_number>\d+)$/, 'GOTO'],
  [/^IF\s+(?<check_var>\w+)\s+THEN\s+(?<then_action>(RIGHT|LEFT|GOTO))($|(?<row_number>\s+\d+)$)/, 'IF'],
]);

const CodeEditorWrapper = styled.div`
  height: 60vh;
  width: 96vw;
  position: relative;
  margin-left: 2vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  gap: 10px;
`;

const CodeEditorBackground = styled.div`
  width: 50%;
  height: 100%;
  background-color: #1F3D21;
  border-radius: 10px;
  padding: 20px 14px;
  overflow: hidden;
`;

const CodeDescription = styled.div`
  width: 50%;
  background-color: #1F3D21;
  color: orange;
  border-radius: 10px;
  padding: 20px;
  height: 100%;
`;

const CodeRowWrapper = styled.label<CodeRowWrapperProps>`
  height: 40px;
  display: grid;
  grid-template-columns: 30px auto 30px;
  border-bottom: ${props => props.valid ? '1px solid green' : '1px solid red'};
`;

const CodeRowWrapperObserver = observer<PropsWithChildren<{ codeRow: CodeRow }>>(({ children, codeRow, ...props }) => (<CodeRowWrapper valid={codeRow.valid}>{children}</CodeRowWrapper>));

const CodeRowNumber = styled.div`
  color: orange;
  font-weight: bold;
  line-height: 40px;
  text-align: right;
  padding: 0 8px;
`;

const CodeRowContentView = styled.input`
  display: block;
  border: none;
  background: inherit;
  color: orange;
  letter-spacing: 4px;
  font-weight: bold;
`;

const CodeRowContent = ({ focus, ...props }: React.InputHTMLAttributes<HTMLInputElement> & { focus: boolean }) => {
  const [input2Ref, setInput2Focus] = UseFocus();
  focus && setInput2Focus();
  useMountEffect(setInput2Focus);
  return (<CodeRowContentView {...props} ref={input2Ref}/>)
}

const DeleteRowButton = styled.button`
  background-color: brown;
  border-radius: 8px;
`;

const CodeRowView = observer<CodeRowProps>(({ focus, number, codeRow, deleteRow, onEnter, program }) => (
  <CodeRowWrapperObserver codeRow={codeRow}>
    <CodeRowNumber>{number + 1}</CodeRowNumber>
    <CodeRowContent
      defaultValue={codeRow.code}
      onChange={(event) => codeRow.setCode(event.target.value)}
      onKeyDown={(event) => event.code === 'Enter' && onEnter()}
      focus={focus}
    />
    {program.rows.length > 1 ? <DeleteRowButton onClick={() => deleteRow()}>&#10005;</DeleteRowButton> : null}
  </CodeRowWrapperObserver>
));

export interface EditorProps {
  readonly program: Program,
}

export default observer<EditorProps>(({ program }) => {
  return (
    <CodeEditorWrapper>
      <CodeEditorBackground>
        {program.rows.map((data, index) => (<CodeRowView
          number={index}
          codeRow={data}
          key={`row-${index}`}
          deleteRow={() => program.deleteRow(index)}
          onEnter={() => program.onEnter(index)}
          focus={index + 1 === program.rows.length}
          program={program}
        />))}
      </CodeEditorBackground>
      <CodeDescription>
        <p>Задачка.
          На дискретной бесконечной прямой на специальных платформах высадились находятся курьер и сборщик.
          Их координаты неизвестны.
          Помогите курьеру встретиться со сборщиком, чтобы сборщик передал заказ для покупателя.
        </p>
        <p>Компилятор понимает следующие команды:</p>
        <ul>
          <li><code>RIGHT</code> - перемещение на один шаг вправо (стоит один тик)</li>
          <li><code>LEFT</code> - перемещение на один шаг влево (стоит один тик)</li>
          <li><code>GOTO #[code row]</code> - перейти к выполнению определенной строки кода</li>
          <li><code>IF ON_PLATFORM THEN (RIGHT|LEFT|GOTO #[code row])</code> - в случае, если курьер/сборщик находится на
            платформе выполняет действие после <code>THEN</code>, иначе преходит к выполнению следующей строки.
            Стоимость действия равна стоимости операции после THEN</li>
        </ul>
      </CodeDescription>
    </CodeEditorWrapper>
  );
});
