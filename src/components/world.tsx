import React, { createRef, forwardRef, PropsWithChildren, Ref, useEffect, useState } from 'react';
import styled from 'styled-components';
import { observer } from 'mobx-react-lite';

import type { Direction } from '../code/robot';
import type { RobotInfo } from '../store/robot-info';
import type { Game } from '../store/game';
import Logo from '../logo.svg';
import Editor from './code-editor';

interface ParachuteProps {
  width: number;
  robot: RobotInfo;
  game: Game;
}

interface ParachuteViewProps {
  width: number;
  location: number;
}

const WorldBackground = styled.div`
  display: block;
  width: 100%;
  height: calc(100% - 6rem);
  position: relative;
  background-image: url("/game-background.webp");
  background-size: contain;
  padding: 3rem 0;
`;

const _WorldContainer = forwardRef((props: PropsWithChildren<any>, ref: Ref<HTMLDivElement>) => (
  <div ref={ref}>
    {props.children}
  </div>
));
const WorldContainer = styled(_WorldContainer)<PropsWithChildren<any>>`
  display: block;
  position: relative;
`;

const RobotContainerView = styled.div<PropsWithChildren<{ width: number; position: number; direction: Direction; image?: string }>>`
  left: ${props => (`${props.position}px`)};
  width: ${props => (`${props.width}px`)};
  border-bottom: ${props => props.image ? 'none' : props.direction === 'front' ? '10px solid black' : '10px solid aqua'};
  border-left: ${props => props.image ? 'none' : props.direction === 'left' ? '10px solid black' : '10px solid aqua'};
  border-right: ${props => props.image ? 'none' : props.direction === 'right' ? '10px solid black' : '10px solid aqua'};
  bottom: 40px;
  display: block;
  position: absolute;
  height: ${props => `${props.width * 2.2}px`};
  background-color: ${props => props.image ? 'none' : 'aqua'};
  background-image: ${props => props.image ? `url(${props.image})` : 'none'};
  background-size: cover;
  transform: ${props => props.direction === 'left' ? 'scaleX(-1)' : 'none'};
  z-index: 2;
`;

const RobotContainer = observer<PropsWithChildren<{ robot: RobotInfo; game: Game; width: number; }>>(
  ({ robot, game, width, children }) => (
    <RobotContainerView
      width={width}
      position={robot.position * game.stepSize}
      direction={robot.direction}
      image={`${robot.image}.webp`}
      title={robot.name}
    />
  ),
);

const Platform = observer<ParachuteProps>(({ robot, game, width }) => (
  <PlatformView width={width} location={robot.initialPosition * game.stepSize}/>
));

const PlatformView = styled.div<ParachuteViewProps>`
  width: ${props => (`${props.width}px`)};
  left: ${props => (`${props.location}px`)};
  height: 40px;
  bottom: 0;
  z-index: 1;
  display: block;
  position: absolute;
  background-image: url("/platform.webp");
`;

const TickButton = styled.button<PropsWithChildren<{ active: boolean }>>`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 30px 90px;
  gap: 10px;

  width: 240px;
  height: 89px;

  background: ${props => props.active ? '#FF9500' : '#1F3D21'};
  border-radius: 10px;

  flex: none;
  order: 0;
  flex-grow: 0;
  cursor: pointer;
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 29px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.04em;
  text-transform: uppercase;

  color: #FFFFFF;
`;

const Label = styled.label`
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 29px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.04em;
  text-transform: uppercase;
  color: #000000;
`;

const Input = styled.input`
  box-sizing: border-box;
  background: #FFFFFF;
  border: 2px solid #1F3D21;
  border-radius: 10px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 30px 50px;
  gap: 10px;
  width: 116px;
  font-size: 24px;
  line-height: 29px;
`;

const WinScreen = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: rgba(0, 0, 0, .7);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const BtnPanel = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding: 0;
  gap: 10px;
  order: 1;
`;

const TopBar = styled.div`
  margin-left: 2vw;
  width: 96vw;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 2vh;
`;

const Ground = styled.div`
  background-image: url("/ground.webp");
  background-repeat: repeat-x;
  height: 40px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`

export default observer<PropsWithChildren<{ game: Game }>>(({ game, children }) => {
  const containerRef = createRef<HTMLDivElement>();

  const [stepsBetweenRobots, setStepsBetweenRobots] = useState(6);
  const stepsOnScreen = 30;
  useEffect(
    () => {
      if (containerRef.current) {
        const mid = stepsOnScreen / 2;
        game.setStepsBetweenRobots(stepsBetweenRobots);
        game.setStepSize(containerRef.current.clientWidth / stepsOnScreen);
        game.init([
          mid + -stepsBetweenRobots / 2,
          mid + stepsBetweenRobots / 2,
        ]);
      }
    },
    [containerRef, stepsBetweenRobots],
  );

  return (
    <WorldBackground>
      <TopBar>
        <BtnPanel>
          <TickButton onClick={() => {
            game.tick();
          }} active={!game.tickOn && game.program.isValid} disabled={game.tickOn && game.program.isValid}>Tik</TickButton>
          <TickButton
            onClick={() => game.toggleTick()}
            active={game.tickOn && game.program.isValid}
          >{game.tickOn ? 'Stop' : 'Start'}</TickButton>
          <TickButton
            onClick={() => {
              game.tickOn && game.toggleTick();
              game.reset();
            }}
            active={game.program.isValid}
            disabled={!game.program.isValid}
          >Reset</TickButton>
        </BtnPanel>
        <div style={{ order: 2, display: 'flex', flexDirection: 'row' }}>
          <Label>Steps between:</Label>
          <Input
            value={stepsBetweenRobots}
            type={'number'}
            onChange={event => setStepsBetweenRobots(+event.target.value)}
            step={2}
            min={2}
            max={8}
          />
        </div>
        <div style={{ order: 3 }}>
          <img src={Logo} alt="X5 Digital"/>
        </div>
      </TopBar>
      <Editor program={game.program} />
      {/*<h1 style={{ color: 'white' }}>TICKS: {game.ticks}</h1>*/}
      {/*<h1 style={{ color: 'white' }}>STEPS BETWEEN ROBOTS: {game.distanceBetweenRobots}</h1>*/}

      {game.robots.isMet && <WinScreen>
        <h1 style={{ color: 'white', width: '40%', textAlign: 'center' }}>Победа!</h1>
        <p style={{ color: 'white' }}>
          Курьер и сборщик встретились за <strong style={{ color: 'orange' }}>{game.ticks}</strong> шагов,
          находясь на расстоянии в <strong style={{ color: 'orange' }}>{game.stepsBetweenRobots}</strong> шагов.
        </p>
        <TickButton
          active={true}
          onClick={() => {
            game.program.clear();
            game.reset();
          }}
        >Again</TickButton>
      </WinScreen>}

      <WorldContainer ref={containerRef}>
        {game.robots.robotsList.map((robot) => (
          <>
            <RobotContainer
              robot={robot}
              game={game}
              width={game.stepSize}
              key={`robot-${robot.name}`}
            >{robot.name}</RobotContainer>
            <Platform robot={robot} game={game} width={game.stepSize}/>
          </>
        ))}
        <Ground />
      </WorldContainer>
    </WorldBackground>
  );
});
